# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField
from wtforms.validators import InputRequired, Email, DataRequired

## login and registration

class LoginN(FlaskForm):
    username = TextField    ('Username', id='username_login'   , validators=[DataRequired()])
    password = PasswordField('Password', id='pwd_login'        , validators=[DataRequired()])
    
class LoginV(FlaskForm):
    username = TextField    ('Username', id='username_login'   , validators=[DataRequired()])
    password = PasswordField('Password', id='pwd_login'        , validators=[DataRequired()])

class CreateNForm(FlaskForm):
    name = TextField('Name'     , id='name_create' , validators=[DataRequired()])
    username = TextField('Username'     , id='username_create' , validators=[DataRequired()])
    email    = TextField('Email'        , id='email_create'    , validators=[DataRequired(), Email()])
    password = PasswordField('Password' , id='pwd_create'      , validators=[DataRequired()])
    pincode = TextField('Pincode'     , id='pincode_create' , validators=[DataRequired()])
    phoneno = TextField('Phone No'     , id='phoneno_create' , validators=[DataRequired()])
    address = TextField('Address'     , id='address_create' , validators=[DataRequired()])

class CreateVForm(FlaskForm):
    name = TextField('Name'     , id='name_create' , validators=[DataRequired()])
    username = TextField('Username'     , id='username_create' , validators=[DataRequired()])
    email    = TextField('Email'        , id='email_create'    , validators=[DataRequired(), Email()])
    password = PasswordField('Password' , id='pwd_create'      , validators=[DataRequired()])
    pincode = TextField('Pincode'     , id='pincode_create' , validators=[DataRequired()])
    phoneno = TextField('Phone No'     , id='phoneno_create' , validators=[DataRequired()])
    address = TextField('Address'     , id='address_create' , validators=[DataRequired()])