# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask import jsonify, render_template, redirect, request, url_for
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)

from app import db, login_manager
from app.base import blueprint
from app.base.forms import LoginN, LoginV, CreateNForm, CreateVForm
from app.base.models import N, V

from app.base.util import verify_pass

@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.home'))

@blueprint.route('/page_<error>')
def route_errors(error):
    return render_template('errors/page_{}.html'.format(error))

## Login & Registration

@blueprint.route('/loginN', methods=['GET', 'POST'])
def loginN():
    login_form = LoginN(request.form)
    if 'login' in request.form:
        
        # read form data
        username = request.form['username']
        password = request.form['password']

        # Locate user
        user = N.query.filter_by(username=username).first()
        
        # Check the password
        if user and verify_pass( password, user.password):

            login_user(user)
            return redirect(url_for('home_blueprint.n_profile'))

        # Something (user or pass) is not ok
        return render_template( 'login/loginn.html', msg='Wrong user or password', form=login_form)

    if not current_user.is_authenticated:
        return render_template( 'login/loginn.html',
                                form=login_form)
    return redirect(url_for('base_blueprint.home'))

    
@blueprint.route('/loginV', methods=['GET', 'POST'])
def loginV():
    login_form = LoginV(request.form)
    if 'login' in request.form:
        
        # read form data
        username = request.form['username']
        password = request.form['password']

        # Locate user
        user = V.query.filter_by(username=username).first()
        
        # Check the password
        if user and verify_pass( password, user.password):

            login_user(user)
            return redirect(url_for('home_blueprint.v_profile'))

        # Something (user or pass) is not ok
        return render_template( 'login/loginv.html', msg='Wrong user or password', form=login_form)

    if not current_user.is_authenticated:
        return render_template( 'login/loginv.html',
                                form=login_form)
    return redirect(url_for('base_blueprint.home'))

@blueprint.route('/create_n', methods=['GET', 'POST'])
def create_n():
    login_form = LoginN(request.form)
    create_account_form = CreateNForm(request.form)
    if 'register' in request.form:

        name  = request.form['name']
        username  = request.form['username']
        email     = request.form['email'   ]
        pincode = request.form['pincode']
        phoneno  = request.form['phoneno']
        address  = request.form['address']
        
        user = N.query.filter_by(username=username).first()
        if user:
            return render_template( 'login/n_register.html', msg='Username already registered', form=create_account_form)

        user = N.query.filter_by(email=email).first()
        if user:
            return render_template( 'login/n_register.html', msg='Email already registered', form=create_account_form)

        # else we can create the user
        user = N(**request.form)
        db.session.add(user)
        db.session.commit()

        return render_template( 'login/n_register.html', success=' created please <a href="/loginN">login</a>', form=create_account_form)

    else:
        return render_template( 'login/n_register.html', form=create_account_form)
    
        
@blueprint.route('/create_v', methods=['GET', 'POST'])
def create_v():
    login_form = LoginV(request.form)
    create_account_form = CreateVForm(request.form)
    if 'register' in request.form:

        name  = request.form['name']
        username  = request.form['username']
        email     = request.form['email'   ]
        pincode = request.form['pincode']
        phoneno  = request.form['phoneno']
        address  = request.form['address']
        
        user = V.query.filter_by(username=username).first()
        if user:
            return render_template( 'login/v_register.html', msg='Username already registered', form=create_account_form)

        user = V.query.filter_by(email=email).first()
        if user:
            return render_template( 'login/v_register.html', msg='Email already registered', form=create_account_form)

        # else we can create the user
        user = V(**request.form)
        db.session.add(user)
        db.session.commit()

        return render_template( 'login/v_register.html', success=' created please <a href="/loginv">login</a>', form=create_account_form)

    else:
        return render_template( 'login/v_register.html', form=create_account_form)

@blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

@blueprint.route('/home')
def home():
    return render_template( 'home.html') 

@blueprint.route('/register')
def register():
    return render_template( 'login/register.html') 
    
@blueprint.route('/login')
def login():
    return render_template( 'login/login.html') 
    
## Errors

@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('errors/page_403.html'), 403

@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('errors/page_403.html'), 403

@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('errors/page_404.html'), 404

@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('errors/page_500.html'), 500
